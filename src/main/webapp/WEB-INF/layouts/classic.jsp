<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><tiles:getAsString name="title"/></title>
    <style>
    	html, body {
			margin: 0;
			padding: 0;
			height: 100%;
		}	
		#container {
			min-height: 100%;
			position: relative;
		}		
		#header {
			border:1px solid #000000;
			background-color:#ffffaa;
		}		
		#body {
			padding: 10px;
			border: 1px solid black;
		}	
		#footer {
			width: 100%;
			position: absolute;
			bottom: 0;
			height: 21pt;
			border:1px solid #000000;
			background-color:#aaffff;
		}
    </style>
  </head>
  <body>
  	<div id="container">
	  	<div id="header">
	  		<tiles:insertAttribute name="header" />
	  	</div>
	    <div id="body">
	    	<tiles:insertAttribute name="body" />
	    </div>
	    <div id="footer">
	    	<tiles:insertAttribute name="footer" />
	    </div>
	</div>
  </body>
</html>