<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@include file="filter-form.jsp"%>

<c:forEach var="news" items="${newsList}" varStatus="status">
	<%@include file="news-header.jsp"%>
</c:forEach>