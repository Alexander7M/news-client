<span style="float:left; font-weight: bold; margin-left: 10px; margin-right:10px; background-color: #CCAAFF;">
	${news.title}
</span>
<span style="float:left; margin-left: 10px; margin-right:10px; background-color: #AAFFCC;">
	(by ${authorMap[news.authorId].name})
</span>
<span style="float:right; margin-left: 10px; margin-right:10px; text-decoration: underline; background-color: #FFAACC;">
	<fmt:formatDate type="date" pattern="dd/MM/yyyy" value="${news.creationDate}" />
</span>
<br/>
<br/>

<span style="float:left; margin-left: 10px; margin-right:10px; background-color: #AACCFF;">
	${news.shortText}
</span>
<br/>

<span style="float:right;">
<span style="color: grey; margin-left: 10px; margin-right:10px; background-color: #FFCCAA;">
	<c:forEach var="tagId" items="${news.tagIdList}" varStatus="status">
		<c:choose>
			<c:when test="${status.count < news.tagIdList.size()}"><c:out value="${tagMap[tagId].name}, "/></c:when>
			<c:otherwise><c:out value="${tagMap[tagId].name}"/></c:otherwise>
		</c:choose>
	</c:forEach>
</span>
<span style="color: red; margin-left: 10px; margin-right:10px; background-color: #CCFFAA;">
	Comments(${news.commentNumber})
</span>
<span style="margin-left: 10px; margin-right:10px; background-color: #AACCAA;">
	<a href="/news-view?newsId=${news.id}">View</a>
</span>
</span>
	
<br/>
<br/>