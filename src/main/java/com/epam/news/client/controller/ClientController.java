package com.epam.news.client.controller;

import static com.epam.news.common.service.util.IdentifiableCollections.hashMap;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news.common.dto.NewsFilter;
import com.epam.news.common.dto.NewsPreview;
import com.epam.news.common.dto.Pagination;
import com.epam.news.common.entity.Author;
import com.epam.news.common.entity.Tag;
import com.epam.news.common.service.AuthorService;
import com.epam.news.common.service.NewsGeneralService;
import com.epam.news.common.service.ServiceException;
import com.epam.news.common.service.TagService;
 
@Controller
public class ClientController {

	static {
		Locale.setDefault(Locale.ENGLISH);
	}

	private static final String NEWS_LIST = "news-list";
	private static final String NEWS_VIEW = "news-view";
	
	@Autowired private NewsGeneralService newsGeneralService;
	@Autowired private AuthorService authorService;
	@Autowired private TagService tagService;
	
	@RequestMapping(value = "/news/list", method = RequestMethod.GET)
	public String newsList(@RequestParam(value="authorId", required=false) Long authorId, ModelMap model) {
		try {
			NewsFilter newsFilter = new NewsFilter(authorId, null);
			Pagination pagination = new Pagination(1);
			List<NewsPreview> newsList = newsGeneralService.getNewsPreviewList(newsFilter, pagination);
			Map<Long, Author> authorMap = hashMap(authorService.getAllAuthors());
			Map<Long, Tag> tagMap = hashMap(tagService.getAllTags());
			model.addAttribute("newsList", newsList);
			model.addAttribute("authorMap", authorMap);
			model.addAttribute("tagMap", tagMap);
		} catch (ServiceException e) {
			model.addAttribute("message", e.getMessage());
		}
		return NEWS_LIST;
	}
	
	@RequestMapping(value = "/news/view")
	public String newsView(@RequestParam("newsId") Long newsId, ModelMap model) {
		return NEWS_VIEW;
	}
	
}